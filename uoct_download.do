
tempfile uocthome off_cit ncic_ucr
tempname uoct
shell curl "https://dps.alaska.gov/Statewide/R-I/UOCT/Home" > `uocthome'

file open `uoct' using `uocthome', read text

file read `uoct' line


while `r(eof)' == 0  {
	
	if `"`OffenseCitations'"' == "" {
		if regexm(`"`line'"', "(\/getmedia\/.*\/OffenseCitations)(.*txt)") {
		local OffenseCitations = "https://dps.alaska.gov" + regexs(1) + ".txt"
		}
	}
	
	if `"`NCIC_UCR'"' == "" {
		if regexm(`"`line'"', "(\/getmedia\/.*\/NCIC_UCRRelationships)(.*txt)") {
		local NCIC_UCR = "https://dps.alaska.gov" + regexs(1) + ".txt"
		}
	}
	
	file read `uoct' line
}

di `"`line'"'
di `"`OffenseCitations'"'
di `"`NCIC_UCR'"'

shell curl "`OffenseCitations'" > "`off_cit'"

shell curl "`NCIC_UCR'" > "`ncic_ucr'"

frame create OffenseCitations
frame create NCIC_UCR

frame OffenseCitations{
	import delim `off_cit' , stringcols(_all)
	rename v1 OffenseCitation
	rename v2 EffectiveBeginDate
	rename v3 EffectiveEndDate
	rename v4 OffenseCategory
	rename v5 OffenseDescription
	rename v6 PrimarySeverity
	rename v7 SecondarySeverity
	rename v8 AttemptedModifier
	rename v9 SolicitationModifier
	rename v10 ConspiracyModifier
	rename v11 GangModifier
	rename v12 CollateralOffenseIndicator
	rename v13 ArtificialOffenseIndicator
	rename v14 APSINOffenseCategory
	rename v15 CourtLiteralDescription
	rename v16 DefaultNCICOffenseCode
	rename v17 NIBRSOffenseCode
	rename v18 DMV
	rename v19 AgencyId
	rename v20 LastUpdateAgencyId
	rename v21 DateAdded
	rename v22 DateLastUpdated
	rename v23 TimeLastUpdated
	
}
	
frame NCIC_UCR {
	import delim `ncic_ucr' , stringcols(_all)
	rename v1 OffenseCitation
	rename v2 EffectiveDate
	rename v3 NCICOffenseCode
	rename v4 UCROffenseCode
	rename v5 AgencyId
	rename v6 LastUpdateAgencyId
	rename v7 DateAdded
	rename v8 DateLastUpdated
	rename v9 TimeLastUpdated
}

/* UCROffenseCode has 4A, 4B, etc which are different types of assault.  Need to reduce to just 4.  Similar with other codes.

/*
import delim "OffenseCitationsHeaders.txt", stringcols(_all)

import delim "OffenseCitations.txt"
